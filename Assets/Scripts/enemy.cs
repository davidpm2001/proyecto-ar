﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    float timespawn;
    public GameObject enemigo;
    void Start()
    {
        StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        if(timespawn== 3)
        {
            GameObject newenemy = Instantiate(enemigo, enemigo.transform.position, enemigo.transform.rotation) as GameObject;
            newenemy.SetActive(true);
            
            timespawn = 0;
        }
    }
    IEnumerator Spawn()
    {
        while(true)
        {
            timespawn = Random.Range(0,10);
            yield return new WaitForSeconds(2.0f);
        }
        
    }
}
